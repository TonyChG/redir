# Redirectionzzzzz

Hellomeo.

## Usage

### Run bruteforce

1. Edit `main.py` and uncomment `bf(a_map)`.
2. Run `main.py`.
 
```bash
python main.py
```

3. Resizing the grid by setting :
- `a_map = Map(7, 7)` in `main.py`
- all 4 generators in `bf.py` :DDDDD

### Manually set portals and print redir

1. Edit `main.py` and uncomment 1 section with 4 calls to `setPortalCell()`
2. Run `main.py`.

```bash
python main.py
```

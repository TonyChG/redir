from enum import Enum


class CellType(Enum):
    PORTAL = "🌌"
    EMPTY = "  "
    BLOCK = "🧊"
    HOLE = "🕳"

from map import Map
from redir import Redir
from position import Position as Pos
from bf import bf


def main():
    a_map = Map(7, 7)

    # bf
    bf(a_map)

    # triple equi test
    # a_map.setPortalCell(Pos(4,4))
    # a_map.setPortalCell(Pos(6,6))
    # a_map.setPortalCell(Pos(2,2))
    # a_map.setPortalCell(Pos(6,7))
    # a_map.draw()
    # r = Redir(a_map.portals, a_map.portals[0])
    # a_map.portals = r.sortedPortals
    # a_map.draw()
    # print(r.size)

    # double equi test
    # a_map.setPortalCell(Pos(0,4))
    # a_map.setPortalCell(Pos(4,4))
    # a_map.setPortalCell(Pos(2,2))
    # a_map.setPortalCell(Pos(0,0))
    # a_map.draw()
    # r = Redir(a_map.portals, a_map.portals[0])
    # a_map.portals = r.sortedPortals
    # a_map.draw()
    # print(r.size)


if __name__ == "__main__":
    main()

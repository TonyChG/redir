from cellType import CellType
from portal import Portal
from position import Position as Pos


class Map:
    def __init__(self, width, height) -> None:
        # define max dimensions for the grid
        self.width = width
        self.height = height
        self.portals = []

        # the grid is a dict containing cells on the map
        # its indexes are the cell coordinate on the map
        # self.grid[0][5] is the cell with abs 0 and ord 5
        self.grid = {}

        # initialize the map grid to empty cells
        for x in range(0, width):
            for y in range(0, height):
                newPos = Pos(x, y)
                self.grid[newPos] = CellType.EMPTY.value

        return None

    def setCell(self, pos: Pos, cellType: CellType) -> None:
        if pos not in self:
            raise ValueError(f"X and Y coordinates must be inside the map")

        # set the grid cell with desired icon
        self.grid[pos] = cellType

        return None

    def setPortalCell(self, pos: Pos) -> None:
        if pos not in self:
            raise ValueError(f"X and Y coordinates must be inside the map")

        # raise an error if there are already 4 portals
        # otherwise, set the grid cell to the correct portal number
        if len(self.portals) >= 4:
            raise ValueError("You already have 4 portals !")
        else:
            self.setCell(pos, CellType.PORTAL.value)
            self.portals.append(Portal(pos, len(self.portals)))

    # used to draw a line above and beneath the grid
    def drawLine(self):
        for x in range(0, self.width):
            print("-" * 6, end="")
        print("-")

    # draw the grid in terminal
    def draw(self):
        self.drawLine()
        for y in range(0, self.height):
            for x in range(0, self.width):
                portal = self.getPortal(Pos(x, y))
                if portal is None:
                    print(f"| {self.grid[Pos(x,y)]}  ", end="")
                else:
                    print(f"| {portal}   ", end="")
            print("|")
        self.drawLine()

    def getPortal(self, pos: Pos):
        for i, portal in enumerate(self.portals):
            if portal.pos == pos:
                return i

    def clearPortals(self):
        for portal in self.portals:
            del portal
        self.portals = []

    def removeLastPortal(self):
        self.grid[self.portals[-1].pos] = CellType.EMPTY.value
        self.portals.pop()

    def __contains__(self, pos: Pos):
        return pos in self.grid.keys()

from position import Position as Pos


class Portal:
    def __init__(self, pos: Pos, index: int):
        self.pos = pos
        self.index = index

    def decrementIndex(self):
        self.index -= 1

    def __repr__(self):
        return f"Portal {self.pos}"

class Position:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def distanceTo(self, otherCell: "Cell"):
        return abs(self.x - otherCell.x) + abs(self.y - otherCell.y)

    def __hash__(self):
        return hash((self.x, self.y))

    def __eq__(self, otherPos):
        return self.x == otherPos.x and self.y == otherPos.y

    def __add__(self, otherPos):
        return Position(self.x + otherPos.x, self.y + otherPos.y)

    def __repr__(self):
        return f"P({self.x},{self.y})"
